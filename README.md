# README #

A simple public example project to test out R test coverage.
Main content comes from the testCoverage project which is BSD licensed: https://github.com/MangoTheCat/testCoverage

To just run the tests:

R CMD check .

To try out testCoverage you can run:

R CMD BATCH coverage.R

### Dependencies ###

```
#!r

install.packages("devtools")
install.packages("testthat")
devtools::install_github("MangoTheCat/testCoverage")
```


on Ubuntu I first had to do:


```
#!bash

sudo apt-get install libcurl4-openssl-dev texlive-fonts-extra
```


Then in R:
```
#!r

install.packages('RCurl')
```
