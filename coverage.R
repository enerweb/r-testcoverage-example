library(testthat)
library(testCoverage)

reportCoverage(packagedir = file.path("."), unittestdir = file.path("tests/testthat"), 
    reportfile = file.path("coverage_report.html"))

#reportCoverage(packagedir = file.path("."),unittestdir = file.path("./inst/tests/testthat/tests1"))
#reportCoverage(sourcefiles = list.files(file.path("R"), full.names = TRUE), executionfiles = list.files(file.path( "inst", "tests", "testthat", "tests1"), full.names = TRUE), reportfile = "testCoverage_saturate_example1.html",writereport = TRUE, clean = TRUE)
